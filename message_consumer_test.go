package hubba

import (
	"fmt"
	"github.com/streadway/amqp"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"testing"
)

type MessageConsumerTestSuite struct {
	suite.Suite
}

func TestMessageConsumerTestSuite(t *testing.T) {
	suite.Run(t, new(MessageConsumerTestSuite))
}

var (
	actionName           = "current.action"
	actionRoutingKey     = "current.action.route1"
	nextActionName       = "next.action"
	nextActionRoutingKey = "next.action.route1"
)

func (s *MessageConsumerTestSuite) TestHandleDeliveryInvalidBody() {
	mockClient, mockStatsD := mockStatsDUnprocessable()
	defer mockStatsD.AssertExpectations(s.T())

	consumer := &messageConsumer{
		hub:    Hub{NextAction: &Exchange{ExchangeName: nextActionName}},
		client: mockClient,
	}
	function := func(a string) ProcessResult { return ProcessResult{Status: StatusSuccess} }
	consumer.handleDelivery(amqp.Delivery{Body: []byte("wrong one!"), RoutingKey: actionRoutingKey}, function)

	mockClient.AssertNotCalled(s.T(), "Publish", mock.Anything, mock.Anything, mock.Anything)
}

func (s *MessageConsumerTestSuite) TestHandleDeliverySuccessWithPublishResult() {
	mockClient, mockStatsD := mockStatsD(StatusSuccess)
	defer mockStatsD.AssertExpectations(s.T())

	hub := Hub{Action: Exchange{RoutingKey: actionRoutingKey}, NextAction: &Exchange{ExchangeName: nextActionName}}
	nextMessage := Message{RequestId: "req-id-next-msg"}

	mockClient.On("Publish", int64(0), *hub.NextAction, nextMessage).Return(nil)
	consumer := &messageConsumer{
		hub:    Hub{NextAction: &Exchange{ExchangeName: nextActionName}},
		client: mockClient,
	}
	function := func(a string) ProcessResult { return ProcessResult{Status: StatusSuccess, NextMessage: &nextMessage} }
	consumer.handleDelivery(amqp.Delivery{Body: []byte("{}"), RoutingKey: actionRoutingKey}, function)

	mockClient.AssertExpectations(s.T())
}

func (s *MessageConsumerTestSuite) TestHandleDeliverySuccessWithoutPublishResult() {
	mockClient, mockStatsD := mockStatsD(StatusSuccess)
	defer mockStatsD.AssertExpectations(s.T())

	consumer := &messageConsumer{
		client: mockClient,
	}
	function := func(a string) ProcessResult { return ProcessResult{Status: StatusSuccess} }
	consumer.handleDelivery(amqp.Delivery{Body: []byte("{}"), RoutingKey: actionRoutingKey}, function)

	mockClient.AssertNotCalled(s.T(), "Publish", mock.Anything, mock.Anything, mock.Anything)
}

func (s *MessageConsumerTestSuite) TestHandleDeliveryUnretriableFailureWithoutPublishResult() {
	mockClient, mockStatsD := mockStatsD(StatusUnRetriableFailure)
	defer mockStatsD.AssertExpectations(s.T())

	hub := Hub{Action: Exchange{RoutingKey: actionRoutingKey}}
	message := Message{RequestId: "req-id-msg"}

	mockClient.On("Publish",
		int64(0),
		Exchange{
			Type: amqp.ExchangeDirect, ExchangeName: "",
			RoutingKey: fmt.Sprintf(deadPattern, hub.Action.RoutingKey),
		},
		message,
	).Return(nil)

	consumer := &messageConsumer{
		hub:    hub,
		client: mockClient,
	}
	function := func(a string) ProcessResult { return ProcessResult{Status: StatusUnRetriableFailure} }
	consumer.handleDelivery(amqp.Delivery{Body: []byte(fmt.Sprintf("{\"request_id\": \"%s\"}", message.RequestId)), RoutingKey: actionRoutingKey}, function)

	mockClient.AssertExpectations(s.T())
}

func (s *MessageConsumerTestSuite) TestHandleDeliveryRetriableFailureWithoutRetrialConfig() {
	mockClient, mockStatsD := mockStatsD(StatusRetriableFailure)
	defer mockStatsD.AssertExpectations(s.T())

	hub := Hub{Action: Exchange{RoutingKey: actionRoutingKey}, NextAction: &Exchange{ExchangeName: nextActionName}}
	nextMessage := Message{RequestId: "req-id-next-msg"}
	mockClient.On("Publish", int64(0), *hub.NextAction, nextMessage).Return(nil)
	consumer := &messageConsumer{
		hub:    hub,
		client: mockClient,
	}
	function := func(a string) ProcessResult {
		return ProcessResult{Status: StatusRetriableFailure, NextMessage: &nextMessage}
	}
	consumer.handleDelivery(amqp.Delivery{Body: []byte("{}"), RoutingKey: actionRoutingKey}, function)

	//expecting publish nextEventName as unretriable failure
	mockClient.AssertExpectations(s.T())
}

func (s *MessageConsumerTestSuite) TestHandleDeliveryRetriableFailureWithRetrialConfig() {
	mockClient, mockStatsD := mockStatsD(StatusRetriableFailure)
	defer mockStatsD.AssertExpectations(s.T())

	delayedSec := int64(30)
	queue := Exchange{RoutingKey: actionRoutingKey}
	retrial := Retrial{DelayIntervals: []int64{delayedSec, delayedSec * 2}}

	mockClient.On("Publish", delayedSec, queue, Message{RetrialCount: int64(1)}).Return(nil)

	consumer := &messageConsumer{
		hub:     Hub{Action: queue},
		client:  mockClient,
		retrial: &retrial,
	}
	function := func(a string) ProcessResult { return ProcessResult{Status: StatusRetriableFailure} }
	consumer.handleDelivery(amqp.Delivery{Body: []byte("{}"), RoutingKey: actionRoutingKey}, function)

	mockClient.AssertExpectations(s.T())
}

func (s *MessageConsumerTestSuite) TestHandleDeliveryRetriableFailureWithRetrialConfigLimitRetrial() {
	mockClient, mockStatsD := mockStatsD(StatusRetriableFailure)
	defer mockStatsD.AssertExpectations(s.T())

	delayedSec := int64(30)
	hub := Hub{Action: Exchange{RoutingKey: actionRoutingKey}, NextAction: &Exchange{ExchangeName: nextActionName}}
	retrial := Retrial{DelayIntervals: []int64{delayedSec, delayedSec * 2}}

	nextMessage := Message{RequestId: "req-id-next-msg"}
	mockClient.On("Publish", int64(0), *hub.NextAction, nextMessage).Return(nil)

	consumer := &messageConsumer{
		hub:     hub,
		client:  mockClient,
		retrial: &retrial,
	}
	function := func(a string) ProcessResult {
		return ProcessResult{
			Status:      StatusRetriableFailure,
			NextMessage: &nextMessage,
		}
	}
	consumer.handleDelivery(amqp.Delivery{Body: []byte("{\"retrial_count\": 2}"), RoutingKey: actionRoutingKey}, function)

	mockClient.AssertExpectations(s.T())
}

func (s *MessageConsumerTestSuite) TestHandleDeliveryDisabledShouldDoNothing() {
	mockClient, mockStatsD := mockStatsD(StatusDisabled)
	defer mockStatsD.AssertExpectations(s.T())

	consumer := &messageConsumer{
		hub:     Hub{Action: Exchange{RoutingKey: actionRoutingKey}, NextAction: &Exchange{ExchangeName: nextActionName}},
		client:  mockClient,
		retrial: &Retrial{DelayIntervals: []int64{1, 2}},
	}

	consumer.handleDelivery(amqp.Delivery{Body: []byte("{}"), RoutingKey: actionRoutingKey}, func(a string) ProcessResult {
		return ProcessResult{Status: StatusDisabled}
	})

	mockClient.AssertNotCalled(s.T(), "Publish", mock.Anything, mock.Anything)
}

func mockStatsD(status string) (*MockClient, MockStatsD) {
	mockStatsD := MockStatsD{}
	mockStatsD.On("Increment", fmt.Sprintf(countRoutingKeyProcessResult, actionRoutingKey, status))
	mockStatsD.On("Increment", fmt.Sprintf(countConsumeRoutingKey, actionRoutingKey))
	mockStatsD.On("Increment", fmt.Sprintf(countAckRoutingKey, actionRoutingKey))
	mockStatsD.On("Gauge", fmt.Sprintf(gaugeAckRoutingKeyDuration, actionRoutingKey), mock.AnythingOfType("int64"))
	mockStatsD.On("Gauge", fmt.Sprintf(gaugeRoutingKeyProcessDuration, actionRoutingKey), mock.AnythingOfType("int64"))

	mockClient := MockClient{}
	mockClient.On("GetStatsD").Return(mockStatsD)

	return &mockClient, mockStatsD
}

func mockStatsDUnprocessable() (*MockClient, MockStatsD) {
	mockStatsD := MockStatsD{}
	mockStatsD.On("Increment", fmt.Sprintf(countRoutingKeyUnprocessablePayload, actionRoutingKey))
	mockStatsD.On("Increment", fmt.Sprintf(countConsumeRoutingKey, actionRoutingKey))
	mockStatsD.On("Increment", fmt.Sprintf(countAckRoutingKey, actionRoutingKey))
	mockStatsD.On("Gauge", fmt.Sprintf(gaugeAckRoutingKeyDuration, actionRoutingKey), mock.AnythingOfType("int64"))

	mockClient := MockClient{}
	mockClient.On("GetStatsD").Return(mockStatsD)

	return &mockClient, mockStatsD
}
