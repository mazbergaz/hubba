package hubba

import (
	"fmt"
	statsdv2 "gopkg.in/alexcesaro/statsd.v2"
	"time"
)

type StatsDClient struct {
	statsD  *statsdv2.Client
	enabled bool
}

type StatsD interface {
	Close()
	Increment(string)
	Gauge(string, interface{})
}

type StatsDConfig struct {
	Prefix string
	Host   string
	Port   int
}

const (
	countChannelOpen                    = "hubba.amqp.channel.open.count"
	countChannelClose                   = "hubba.amqp.channel.close.count"
	countPublishExchangeRoutingKey      = "hubba.amqp.publish.%s.%s.%s.count"
	countConsumeRoutingKey              = "hubba.amqp.consume.%s.count"
	gaugeAckRoutingKeyDuration          = "hubba.amqp.ack.%s.duration"
	countAckRoutingKey                  = "hubba.amqp.ack.%s.count"
	countRoutingKeyUnprocessablePayload = "hubba.%s.unprocessable.count"
	countRoutingKeyProcessResult        = "hubba.%s.result.%s.count"
	gaugeRoutingKeyProcessDuration      = "hubba.%s.process.duration"
)

func InitiateStatsD(config StatsDConfig) StatsDClient {
	if config.Host == "" || config.Port == 0 {
		return StatsDClient{}
	}
	address := fmt.Sprintf("%s:%d", config.Host, config.Port)
	statsD, err := statsdv2.New(statsdv2.Address(address), statsdv2.Prefix(config.Prefix))
	if err != nil {
		return StatsDClient{}
	}
	return StatsDClient{statsD: statsD, enabled: true}
}

func (s StatsDClient) Close() {
	if s.enabled {
		s.statsD.Close()
	}
}

func (s StatsDClient) Increment(key string) {
	if s.enabled {
		s.statsD.Increment(key)
	}
	return
}

func (s StatsDClient) Gauge(key string, value interface{}) {
	if s.enabled {
		s.statsD.Gauge(key, value)
	}
}

func currentTimeStampMillisecond() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}
