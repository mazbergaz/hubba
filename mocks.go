package hubba

import (
	"github.com/stretchr/testify/mock"
)

/* AMQP CLIENT */
type MockClient struct {
	mock.Mock
}

func (m *MockClient) Publish(delayMillis int64, exchange Exchange, message Message) error {
	args := m.Called(delayMillis, exchange, message)
	return args.Error(0)
}

func (m *MockClient) Subscribe(hub Hub, fn process, retrial *Retrial, channel ChannelConfig) error {
	args := m.Called(hub, fn, retrial, channel)
	return args.Error(0)
}

func (m *MockClient) CloseConnection() error {
	args := m.Called()
	return args.Error(0)
}

func (m *MockClient) GetStatsD() StatsD {
	args := m.Called()
	return args.Get(0).(StatsD)
}

/* STATSD CLIENT */
type MockStatsD struct {
	mock.Mock
}

func (m MockStatsD) Close() {
	m.Called()
}

func (m MockStatsD) Increment(key string) {
	m.Called(key)
}

func (m MockStatsD) Gauge(key string, value interface{}) {
	m.Called(key, value)
}
