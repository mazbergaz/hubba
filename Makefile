ALL_PACKAGES=$(shell go list ./... | grep -v "vendor")

setup:
	go get -u github.com/golang/lint/golint

build-deps:
	glide install

build: build-deps fmt lint

fmt:
	go fmt $(GLIDE_NOVENDOR)

lint:
	@for p in $(UNIT_TEST_PACKAGES); do \
		echo "==> Linting $$p"; \
		golint $$p | { grep -vwE "exported (var|function|method|type|const) \S+ should have comment" || true; } \
	done

test-cover-html:
	echo "mode: count" > out/coverage-all.out
	$(foreach pkg,$(ALL_PACKAGES),\
		go test -coverprofile=out/coverage.out -covermode=count $(pkg);\
		tail -n +2 out/coverage.out >> out/coverage-all.out;)
		go tool cover -html=out/coverage-all.out -o out/coverage.html
		go tool cover -func=out/coverage-all.out

test:
	go test $(ALL_PACKAGES) -p=1 -race
