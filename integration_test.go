package hubba

import (
	"github.com/streadway/amqp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"log"
	"testing"
	"time"
)

type IntegrationTestSuite struct {
	suite.Suite
}

func TestIntegrationTestSuite(t *testing.T) {
	suite.Run(t, new(IntegrationTestSuite))
}

const (
	rabbitMQHost         = "localhost"
	rabbitMQPort         = 5672
	rabbitMQUser         = ""
	rabbitMQPassword     = ""
	channelPrefetchCount = 5
	channelPrefetchSize  = 0
	channelConfigGlobal  = false
	statsdPrefix         = "test"
	statsdHost           = "localhost"
	statsdPort           = 8125
)

func getAmqpConfig() AmqpConfig {
	return AmqpConfig{
		Host:     rabbitMQHost,
		Port:     rabbitMQPort,
		User:     rabbitMQUser,
		Password: rabbitMQPassword,
	}
}

func getChannelConfig() ChannelConfig {
	return ChannelConfig{
		PrefetchCount: channelPrefetchCount,
		PrefetchSize:  channelPrefetchSize,
		Global:        channelConfigGlobal,
	}
}

func getStatsDConfig() *StatsDConfig {
	return &StatsDConfig{
		Prefix: statsdPrefix,
		Host:   statsdHost,
		Port:   statsdPort,
	}
}

func (s IntegrationTestSuite) TestInitPublisher() {
	publisher, err := Init(getAmqpConfig(), getStatsDConfig())
	assert.Nil(s.T(), err)
	assert.NotNil(s.T(), publisher)

	defer publisher.CloseConnection()
}

func (s IntegrationTestSuite) TestClosePublisherConnection() {
	publisher, _ := Init(getAmqpConfig(), getStatsDConfig())
	err := publisher.CloseConnection()
	assert.Nil(s.T(), err)

	_, err = publisher.connection.Channel()
	assert.Equal(s.T(), amqp.ErrClosed, err)
}

func (s IntegrationTestSuite) TestInitSubscriber() {
	subscriber, err := Init(getAmqpConfig(), getStatsDConfig())
	assert.Nil(s.T(), err)
	assert.NotNil(s.T(), subscriber)

	defer subscriber.CloseConnection()
}

func (s IntegrationTestSuite) TestCloseSubscriberConnection() {
	subscriber, _ := Init(getAmqpConfig(), getStatsDConfig())
	err := subscriber.CloseConnection()
	assert.Nil(s.T(), err)

	_, err = subscriber.connection.Channel()
	assert.Equal(s.T(), amqp.ErrClosed, err)
}

func (s IntegrationTestSuite) TestPublishFanout() {
	client, _ := Init(getAmqpConfig(), getStatsDConfig())
	defer client.CloseConnection()

	exchangeName := "exchange.test.1"
	subscriberRoutingKey := "route.key.1"
	err := client.Publish(0, Exchange{ExchangeName: exchangeName, Type: amqp.ExchangeFanout}, Message{Payload: "{\"test\":true}"})
	assert.Nil(s.T(), err)

	fn := func(a string) ProcessResult {
		log.Printf("received %s\n", a)
		return ProcessResult{Status: StatusSuccess}
	}

	go func() {
		err = client.Subscribe(Hub{Action: Exchange{ExchangeName: exchangeName, RoutingKey: subscriberRoutingKey, Type: amqp.ExchangeFanout}}, fn, nil, getChannelConfig())
		assert.Nil(s.T(), err)
	}()

	time.Sleep(100 * time.Millisecond)

	//TODO: assert no message in exchangeName and routingKey
}

func (s IntegrationTestSuite) TestPublishDirectWithDelay() {
	//TODO
}

func (s IntegrationTestSuite) TestPublishDirectWithoutDelay() {
	//TODO
}
