package hubba

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"log"
)

type messageConsumer struct {
	hub     Hub
	retrial *Retrial
	client  Client
}

func (c *messageConsumer) listen(ch *amqp.Channel, autoAck bool, fn process) error {
	msgs, err := ch.Consume(c.hub.Action.RoutingKey, "", autoAck, false, false, false, nil)
	if err != nil {
		return err
	}

	forever := make(chan bool)
	go func() {
		for d := range msgs {
			c.handleDelivery(d, fn)
		}
	}()
	log.Printf("listening to %s", c.hub.Action.RoutingKey)
	<-forever
	return nil
}

func (c *messageConsumer) handleDelivery(d amqp.Delivery, function process) {
	c.client.GetStatsD().Increment(fmt.Sprintf(countConsumeRoutingKey, d.RoutingKey))
	startTime := currentTimeStampMillisecond()
	defer c.ack(d, startTime)

	actionMessage := Message{}
	err := json.Unmarshal(d.Body, &actionMessage)
	if err != nil {
		c.client.GetStatsD().Increment(fmt.Sprintf(countRoutingKeyUnprocessablePayload, d.RoutingKey))
		log.Printf("Unproccessable payload: '%s'", string(d.Body))
		return
	}

	result := c.executeFunction(d.RoutingKey, function, actionMessage.Payload)

	switch result.Status {
	case StatusDisabled:
		log.Println("Status disabled, do nothing.")
	case StatusSuccess:
		c.publishNextAction(result.NextMessage)
	case StatusUnRetriableFailure:
		c.handleUnRetriable(result, actionMessage)
	case StatusRetriableFailure:
		if c.retrial != nil && int(actionMessage.RetrialCount) < len(c.retrial.DelayIntervals) {
			c.publishRetry(actionMessage)
		} else {
			c.handleUnRetriable(result, actionMessage)
		}
	default:
		log.Printf("Unknown status '%s', do nothing.", result.Status)
	}
}

func (c *messageConsumer) executeFunction(routingKey string, fn process, payload string) ProcessResult {
	startTime := currentTimeStampMillisecond()
	result := fn(payload)
	c.client.GetStatsD().Gauge(fmt.Sprintf(gaugeRoutingKeyProcessDuration, routingKey), currentTimeStampMillisecond()-startTime)
	c.client.GetStatsD().Increment(fmt.Sprintf(countRoutingKeyProcessResult, routingKey, result.Status))
	return result
}

func (c *messageConsumer) ack(d amqp.Delivery, startTimeStamp int64) error {
	c.client.GetStatsD().Increment(fmt.Sprintf(countAckRoutingKey, d.RoutingKey))
	c.client.GetStatsD().Gauge(fmt.Sprintf(gaugeAckRoutingKeyDuration, d.RoutingKey), currentTimeStampMillisecond()-startTimeStamp)
	return d.Ack(false)
}

func (c *messageConsumer) handleUnRetriable(result ProcessResult, actionMessage Message) error {
	if c.hub.NextAction == nil || result.NextMessage == nil {
		return c.client.Publish(
			0,
			Exchange{Type: amqp.ExchangeDirect, ExchangeName: "", RoutingKey: fmt.Sprintf(deadPattern, c.hub.Action.RoutingKey)},
			actionMessage,
		)
	}
	return c.publishNextAction(result.NextMessage)
}

func (c *messageConsumer) publishNextAction(message *Message) error {
	if c.hub.NextAction != nil && message != nil {
		return c.client.Publish(
			0,
			*c.hub.NextAction,
			*message,
		)
	}
	return nil
}

func (c *messageConsumer) publishRetry(message Message) error {
	message.RetrialCount = message.RetrialCount + 1
	currentDelay := c.currentDelay(message.RetrialCount)
	return c.client.Publish(
		currentDelay,
		c.hub.Action,
		message,
	)
}

func (c *messageConsumer) currentDelay(retrialCount int64) int64 {
	current := int(retrialCount)
	if c.retrial == nil || current > len(c.retrial.DelayIntervals) {
		return int64(-1)
	}
	if current > 0 {
		return c.retrial.DelayIntervals[current-1]
	}
	return int64(0)
}
