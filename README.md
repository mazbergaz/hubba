# HUBBA
Go Framework for routing processes with async communication and built on top of AMQP.
This framework supports processing flows which based on:
- publishing message (direct/fanout) with/without delays
- subscribing message with function to be executed when message is received
- push message to dead letter or to next event or setting retrial after process is executed based on configuration and result status.
- statsd metrics (if enabled) to monitor some amqp basic stuffs (which can't be retrieved from rabbitmq directly) and process result:
    - count channel opened/closed: `hubba.amqp.channel.open.count` and `hubba.amqp.channel.close.count`
    - count messages successfully or failed published to each exchange/routing key `hubba.amqp.publish.<exchangeName>.<routingKey>.<success/failed>.count`
    - count messages consumed and acked on each routing key `hubba.amqp.consume.<routingKey>.count` and `hubba.amqp.ack.<routingKey>.count`
    - gauge ack duration on each routing key `hubba.amqp.ack.%s.duration`
    - count unprocessable payload on each routing key `hubba.<routingKey>.unprocessable.count`
    - count each result status on each routing key `hubba.<routingKey>.result.<status>.count`
    - gauge process duration on each routing key `hubba.<routingKey>.process.duration`

## Usage
### Initialize
```
hubba, err := hubba.Init(
    AmqpConfig{
        Host:     amqpHost,
        Port:     amqpPort,
        User:     amqpUser,
        Password: amqpPassword,
        Channel: ChannelConfig{
            PrefetchCount: channelPrefetchCount,
            PrefetchSize:  channelPrefetchSize,
            Global:        channelConfigGlobal,
        }
    },
)

// with statsD client
hubba.WithStatsD(
    StatsDConfig{
        Prefix: amqpPrefix,
        Host: statsdHost,
        Port: statsdPort,
    },
)

// on shutdown
hubba.CloseConnection()
```

### Publisher
#### Fanout Publishing
```
exchangeName := "example-exchange"
delayMillis := int64(100)
err := hubba.Publish(
    delayMillis,
    Exchange{
        ExchangeName: exchangeName,
        Type: amqp.ExchangeFanout,
    },
    Message{
        RequestId: "to map published&subscribed message for easier logging or other context",
        Payload: "{\"test\":true}",
    },
)
```
Fanout publish with delay will create an exchange with template `fanout-<exchangeName>-delay-<delayMillis>` with x-dead-letter-exchange: exchangeName.

#### Direct Publishing
```
routingKey := "example-routing-key"
delayMillis := int64(100)
err := hubba.Publish(
    delayMillis,
    Exchange{
        RoutingKey: routingKey,
        Type: amqp.ExchangeFanout,
    },
    Message{
        RequestId: "to map published&subscribed message for easier logging or other context",
        Payload: "{\"test\":true}",
    },
)
```
Direct publish with delay will create an exchange with template `direct-<routingKey>-delay-<delayMillis>` with x-dead-letter-exchange: "" and x-dead-letter-routing-key: routingKey

### Subscriber
In subscribe there is a concept of `Hub` which determine the current exchange/queue to be processed and also to set the next action to publish.
```
function := func(a string) ProcessResult {
    return ProcessResult{
        Status: StatusSuccess,
        NextMessage: &Message{                   // <- to set the message for the next event publishing
            RequestId: "request-id-example",
            Payload: "{\"test\":true}",
        },
    }
}

err := hubba.Subscribe(
    Hub{
        Action: Exchange{
            ExchangeName: exchangeName,
            RoutingKey: subscriberRoutingKey,
            Type: amqp.ExchangeFanout,
        },
        NextAction: &Exchange{                  // <- exchange to be published when getting result StatusSuccess or StatusUnRetriableFailure. will only be sent if NextMessage != nil
            ExchangeName: nextExchangeName,
            Type: amqp.ExchangeFanout,
        },
    },
    function,
    &Retrial{DelayIntervals: []int64{1, 2}},    // <- retry intervals (millisecond).
)
```
Retrial will only be executed if under max retrial limit (based on DelayIntervals length) and the function returns ProcessResult with status StatusRetriableFailure.
If NextAction is not specified, hubba by default will publish the message to a dead letter queue with pattern `<routingKey>-dead` when function returns StatusUnRetriablefailure.

## Install
go get git@gitlab.com:mazbergaz/hubba

## Local Setup
### Pre Requisites
- rabbitmq

### Install Dependencies
```
$ glide install
```

### Run Test
start rabbitmq
```
$ make test
```
