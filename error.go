package hubba

import "errors"

var (
	ConnectionError  = errors.New("connection error")
	UnknownTypeError = errors.New("unknown exchange type")
)
