module gitlab.com/mazbergaz/hubba

go 1.16

require (
	github.com/streadway/amqp v0.0.0-20180112231532-a354ab84f102
	github.com/stretchr/testify v1.7.0
	gopkg.in/alexcesaro/statsd.v2 v2.0.0
)
