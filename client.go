package hubba

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"log"
)

type Retrial struct {
	DelayIntervals []int64
}

type Hub struct {
	Action     Exchange
	NextAction *Exchange
}

type Exchange struct {
	Type         string
	ExchangeName string
	RoutingKey   string
}

type AmqpConfig struct {
	Host     string
	Port     int
	User     string
	Password string
}

type ChannelConfig struct {
	PrefetchCount int
	PrefetchSize  int
	AutoAck       bool
	Global        bool
}

type Client interface {
	Publish(delayInSecond int64, exchange Exchange, message Message) error
	Subscribe(hub Hub, fn process, retrial *Retrial, config ChannelConfig) error
	CloseConnection() error
	GetStatsD() StatsD
}

type Hubba struct {
	connection *amqp.Connection
	statsD     StatsD
}

const (
	delayPattern = "%s-%s-delay-%d"
	deadPattern  = "%s-dead"
)

type process func(string) ProcessResult

func Init(config AmqpConfig) (*Hubba, error) {
	conn, err := createConnection(config)
	if err != nil {
		return nil, err
	}
	hubba := &Hubba{
		connection: conn,
	}
	return hubba, nil
}

func (s *Hubba) WithStatsD(statsDConfig StatsDConfig) *Hubba {
	s.statsD = InitiateStatsD(statsDConfig)
	return s
}

func createConnection(config AmqpConfig) (*amqp.Connection, error) {
	var url string
	if config.User != "" && config.Password != "" {
		url = fmt.Sprintf("amqp://%s:%s@%s:%d", config.User, config.Password, config.Host, config.Port)
	} else {
		url = fmt.Sprintf("amqp://%s:%d", config.Host, config.Port)
	}
	return amqp.Dial(url)
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func (s *Hubba) getChannel() *amqp.Channel {
	ch, err := s.connection.Channel()
	failOnError(err, "Failed to open a channel")
	s.statsD.Increment(countChannelOpen)
	return ch
}

func (s *Hubba) getChannelWithConfig(channelConfig ChannelConfig) *amqp.Channel {
	ch := s.getChannel()
	if ch != nil {
		ch.Qos(channelConfig.PrefetchCount, channelConfig.PrefetchSize, channelConfig.Global)
	}
	return ch
}

func (s *Hubba) CloseConnection() error {
	s.statsD.Close()
	return s.connection.Close()
}

func (s *Hubba) Publish(delayMillis int64, exchange Exchange, message Message) error {
	ch := s.getChannel()
	defer s.closeChannel(ch)

	if delayMillis > int64(0) {
		var exchangeName string
		if exchange.Type == amqp.ExchangeFanout {
			exchangeName = s.setupFanoutDelay(delayMillis, exchange, ch)
		} else if exchange.Type == amqp.ExchangeDirect {
			exchangeName = s.setupDirectDelay(delayMillis, exchange, ch)
		}
		return s.publish(exchangeName, "", message, ch)
	}

	if exchange.Type == amqp.ExchangeFanout {
		err := ch.ExchangeDeclare(exchange.ExchangeName, amqp.ExchangeFanout, true, false, false, false, nil)
		failOnError(err, fmt.Sprintf("Failed to declare exchange %s", exchange.ExchangeName))
		return s.publish(exchange.ExchangeName, "", message, ch)
	} else if exchange.Type == amqp.ExchangeDirect {
		_, err := ch.QueueDeclare(exchange.RoutingKey, true, false, false, false, nil)
		failOnError(err, fmt.Sprintf("Failed to declare queue %s", exchange.RoutingKey))
		return s.publish("", exchange.RoutingKey, message, ch)
	}

	return UnknownTypeError
}

func (s *Hubba) closeChannel(ch *amqp.Channel) error {
	s.statsD.Increment(countChannelClose)
	return ch.Close()
}

func (s *Hubba) setupDirectDelay(delayMillis int64, e Exchange, ch *amqp.Channel) string {
	delayExchangeName := fmt.Sprintf(delayPattern, e.Type, e.RoutingKey, delayMillis)

	err := ch.ExchangeDeclare(delayExchangeName, amqp.ExchangeDirect, true, false, false, false,
		s.delayArgs(delayMillis, "", e.RoutingKey))
	failOnError(err, fmt.Sprintf("Failed to declare exchange %s", delayExchangeName))

	_, err = ch.QueueDeclare(e.RoutingKey, true, false, false, false, nil)
	failOnError(err, fmt.Sprintf("Failed to declare queue %s", e.RoutingKey))

	return delayExchangeName
}

func (s *Hubba) setupFanoutDelay(delayMillis int64, e Exchange, ch *amqp.Channel) string {
	delayExchangeName := fmt.Sprintf(delayPattern, e.Type, e.ExchangeName, delayMillis)

	err := ch.ExchangeDeclare(delayExchangeName, amqp.ExchangeFanout, true, false, false, false,
		s.delayArgs(delayMillis, e.ExchangeName, ""))
	failOnError(err, fmt.Sprintf("Failed to declare exchange %s", delayExchangeName))

	err = ch.ExchangeDeclare(e.ExchangeName, amqp.ExchangeFanout, true, false, false, false, nil)
	failOnError(err, fmt.Sprintf("Failed to declare exchange %s", e.ExchangeName))

	return delayExchangeName
}

func (s *Hubba) delayArgs(delayMillis int64, exchangeName, routingKey string) map[string]interface{} {
	return map[string]interface{}{
		"x-message-ttl":             delayMillis,
		"x-dead-letter-exchange":    exchangeName,
		"x-dead-letter-routing-key": routingKey,
	}
}

func (s *Hubba) publish(exchangeName, routingKey string, message Message, ch *amqp.Channel) error {
	messageByte, _ := json.Marshal(&message)
	err := ch.Publish(
		exchangeName,
		routingKey,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        messageByte,
		})
	result := "success"
	if err != nil {
		result = "failed"
	}
	s.statsD.Increment(fmt.Sprintf(countPublishExchangeRoutingKey, exchangeName, routingKey, result))
	return err
}

func (s *Hubba) Subscribe(hub Hub, fn process, retrial *Retrial, channelConfig ChannelConfig) error {
	ch := s.getChannel()
	defer s.closeChannel(ch)

	err := ch.ExchangeDeclare(hub.Action.ExchangeName, amqp.ExchangeFanout, true, false, false, false, nil)
	failOnError(err, fmt.Sprintf("Failed to declare exchange %s", hub.Action.ExchangeName))

	_, err = ch.QueueDeclare(hub.Action.RoutingKey, true, false, false, false, nil)
	failOnError(err, fmt.Sprintf("Failed to declare queue %s", hub.Action.RoutingKey))

	err = ch.QueueBind(hub.Action.RoutingKey, hub.Action.RoutingKey, hub.Action.ExchangeName, false, nil)
	failOnError(err, fmt.Sprintf("Failed to bind queue %s to exchange %s", hub.Action.RoutingKey, hub.Action.ExchangeName))

	consumer := &messageConsumer{
		hub:     hub,
		retrial: retrial,
		client:  s,
	}
	return consumer.listen(ch, false, fn)
}

func (s *Hubba) GetStatsD() StatsD {
	return s.statsD
}
