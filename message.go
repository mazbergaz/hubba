package hubba

type Message struct {
	RequestId    string `json:"request_id"`
	Payload      string `json:"payload"`
	RetrialCount int64  `json:"retrial_count"`
}

type ProcessResult struct {
	Status       string
	ErrorMessage string
	NextMessage  *Message
}

const (
	StatusSuccess            = "SUCCESS"
	StatusRetriableFailure   = "RETRIABLE_FAILURE"
	StatusUnRetriableFailure = "UNRETRIABLE_FAILURE"
	StatusDisabled           = "DISABLED"
)
